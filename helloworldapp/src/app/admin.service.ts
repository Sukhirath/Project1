import { Injectable } from '@angular/core';

@Injectable()
export class AdminService {

  private AdminLoggedIn;
  constructor() { 
  	this.AdminLoggedIn=false;
  }

  setAdminLoggedIn(){
  	if(this.AdminLoggedIn==false)
  		this.AdminLoggedIn=true;
  	else
  		this.AdminLoggedIn=false;
  }

}
