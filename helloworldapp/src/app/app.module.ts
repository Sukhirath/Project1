import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';
import { AddBookComponent } from './add-book/add-book.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';



const appRoutes:Routes=[
  {
    path:'login',
    component: LoginFormComponent
  },
  {
    path:'register',
    component: RegisterComponent
  },
  {
    path:'user',
    component: UserComponent
  },
  {
    path:'admin',
    component: AdminComponent
  },
  {
    path: 'add-book',
    component: AddBookComponent
  },
  {
    path:'',
    component: HomeComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginFormComponent,
    FooterComponent,
    HomeComponent,
    RegisterComponent,
    UserComponent,
    AdminComponent,
    AddBookComponent,
    AdminHeaderComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
