import { Component, OnInit } from '@angular/core';
import { Router }  from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  constructor(private router:Router) { }

  ngOnInit() {
  }

  regUser(e)
  {
  	e.preventDefault();
  	var name = e.target.elements[0].value;
  	var username = e.target.elements[1].value;
  	var email = e.target.elements[2].value;
    var re_email = e.target.elements[3].value;
    var pass = e.target.elements[4].value;
    if(email==re_email)
    {
      this.router.navigate(['login']);
    }

  }

 
}
